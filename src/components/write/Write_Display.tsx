import React from 'react';
import { InputBox, Pannel, PoemArea, RegisterButton, WriteDisplayButtonComponent, WriteDisplayContentComponent, WriteDisplayInputComponent, WriteDisplayMainComponent, WriteDisplayTitleComponent } from "./style/WriteDisplayStyled";
import { useMutation } from 'react-query';
import { postData } from '../../service/APIEngine';

interface Props {
    visible: boolean;
    setVisible: React.Dispatch<React.SetStateAction<boolean>>;
    title: string;
    setTitle: React.Dispatch<React.SetStateAction<string>>;
    writer: string;
    setWriter: React.Dispatch<React.SetStateAction<string>>;
    content: string;
    setContent: React.Dispatch<React.SetStateAction<string>>;
}

const WriteDisplay = ({
    visible, 
    setVisible,
    title,  
    setTitle,
    writer,  
    setWriter,
    content,  
    setContent
}: Props) => {

    const {mutate: writeMutate } = useMutation( 
        async () =>  await postData(
            '/write-poem',
            {
                title: title,
                writer: writer,
                content: content,
                id: 'skam123',
            },
            false,
            '',
        ),
        {
            onSuccess: () => {
                setVisible(false);
            }
        }
    )

    const registerPoem  = () => {
        writeMutate();
    }
    
    return (
        <WriteDisplayMainComponent visible={visible}>
            <WriteDisplayTitleComponent>
                <Pannel>새로운 시 등록하기</Pannel>
            </WriteDisplayTitleComponent>
            <WriteDisplayInputComponent>
                <Pannel fontColor={'black'}>제목</Pannel>
                <InputBox onChange={(event) => setTitle(event.target.value)} type="text" placeholder="제목을 입력해주세요." />
            </WriteDisplayInputComponent>
            <WriteDisplayInputComponent>
                <Pannel fontColor={'black'}>저자</Pannel>
                <InputBox onChange={(event) => setWriter(event.target.value)} type="text" placeholder="저자를 입력해주세요." />
            </WriteDisplayInputComponent>
            <WriteDisplayContentComponent>
                <PoemArea onChange={(event) => setContent(event.target.value)} placeholder="시의 내용을 입력하세요." />
            </WriteDisplayContentComponent>
            <WriteDisplayButtonComponent>
                <RegisterButton onClick={() => registerPoem()}>시 등록</RegisterButton>
            </WriteDisplayButtonComponent>
        </WriteDisplayMainComponent>
    );
}

export default WriteDisplay;