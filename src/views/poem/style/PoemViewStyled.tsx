import styled from "styled-components";
import styles from "styled-components";
import { GoHeart } from "react-icons/go";
import { GoHeartFill } from "react-icons/go";

export const PoemViewMainComponent = styled.div`
    position: relative;
    width: 100%;
    height: 100%;
    background-color: black;
    display: flex;
    flex-direction: column;
`;

export const PoemViewBodyComponent = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
`;

export const PoemViewMainPoemContent = styled.div`
    position: relative;
    width: 50%;
    height: 100%;
    background-color: black;
    overflow-y: auto;
    &::-webkit-scrollbar {
        background-color: black;
    }
`;

export const PoemViewTitle = styled.p<{
    length?: number;
}>`
    position: absolute;
    font-size: 2.5vh;
    font-weight: 700;
    top: 8%;
    left: 20vh;
    color: white;
`;

export const PeomViewWriter = styled.p`
    position: absolute;
    font-size: 2.5vh;
    font-weight: 700;
    top: 15%;
    left: 50vh;
    color: white;
`;

export const PoemViewContent = styled.pre`
    width: 100%;
    position: absolute;
    font-size: 2.5vh;
    font-weight: 200;
    top: 20%;
    left: 2vh;
    background-color: black;
    color: white;
`;
export const PoemViewImage = styled.div<{
    imagePath? : string;
}>`
    position: relative;
    width: 50%;
    height: 100%;
    background-image: ${(props: any) => `url(${props.imagePath})`};
    background-size: 100% 100%;
    background-repeat: no-repeat;
    &::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: -5vh;
    width: 50%;
    height: 100%;
    background-image: linear-gradient(-90deg, transparent 0 55%, black 85% 90%);
  }
`;

export const PoemGoodsFalseHeart = styles(GoHeart)`
  position: absolute;
  z-index: 999;
  font-size: 3.5vh;
  font-weight: 900;
  right: 2.5vh;
  top: 10%;
  color: red;
`

export const PoemGoodsTrueHeart = styles(GoHeartFill)`
  position: absolute;
  z-index: 999;
  font-size: 3.5vh;
  font-weight: 900;
  right: 2.5vh;
  top: 10%;
  color: red;
`
