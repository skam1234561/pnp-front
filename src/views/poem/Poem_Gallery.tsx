import React, { useCallback, useEffect, useState } from 'react';
import Header from "../../components/header/Header";
import { CurrentPagingNumber, LeftArrow, PagingNumber, PoemGalleryBodyComponent, PoemGalleryBodyInnerComponent, PoemGalleryMainComponent, PoemTable, PoemTableData, PoemTableHead, PoemTableRow, RightArrow, TablePagingComponent, TablePagingNumberComponent, WritePoemButton } from "./style/PoemGalleryStyled";
import { BiPencil } from "react-icons/bi";
import WriteDisplay from '../../components/write/Write_Display';
import { useQuery } from 'react-query';
import { fetchData } from '../../service/APIEngine';
import { useNavigate } from 'react-router-dom';
import { usePoemSeqContext } from '../../store/context/PoemContext';
import moment from 'moment';
const PoemGallery = () => {
    const poemSeqContext = usePoemSeqContext();
    const navigate = useNavigate();
    const [currentPage, setCurrentPage] = useState(1);
    const [visible, setVisible] = useState(false);
    const [title, setTitle] = useState('');
    const [writer, setWriter] = useState('');
    const [content, setContent] = useState('');
    const [paging, setPaging] = useState<number[]>([]);


    const goPoemView = (poemSeq: number) => {
        navigate('/poem-view?poemSeq=' + poemSeq);
    }

    const {
        data: poemData,
        error: poemError,
        isError: poemIsError,
        refetch: poemRefetch,
    } = useQuery(['read-paging-poem', [visible, currentPage]], async () => {
        let poems = await fetchData(
            {
                id: 'skam123',
                page: currentPage,
            },
            '/read-paging-poem',
        );

        return poems?.data;
    });

    useEffect(() => {
        let pagingList: number[] = [];
        for(let i = poemData?.first_paging; i <= poemData?.last_paging; i++) {
            pagingList.push(i);
        }
        setPaging(pagingList);

    }, [poemData])

    const viewWritePoems = useCallback(() => {
        if(poemData?.poem?.length === 0) { 
            return (
                <PoemTable>
                    <PoemTableRow height={2}>
                        <PoemTableHead width={3}>순번</PoemTableHead>
                        <PoemTableHead width={20}>제목</PoemTableHead>
                        <PoemTableHead width={5}>저자</PoemTableHead>
                        <PoemTableHead width={6}>등록일자</PoemTableHead>
                        <PoemTableHead width={3}>조회</PoemTableHead>
                        <PoemTableHead width={3}>좋아요</PoemTableHead>
                    </PoemTableRow>
                    <PoemTableRow height={40}>      
                        <PoemTableData colSpan={6} style={{textAlign: 'center'}}>-----------등록된 시가 없습니다-----------</PoemTableData>
                    </PoemTableRow>
                </PoemTable>
            );

        }
        return (
            <PoemTable>
                    <PoemTableRow height={2}>
                        <PoemTableHead width={3}>순번</PoemTableHead>
                        <PoemTableHead width={20}>제목</PoemTableHead>
                        <PoemTableHead width={5}>저자</PoemTableHead>
                        <PoemTableHead width={6}>등록일자</PoemTableHead>
                        <PoemTableHead width={3}>조회</PoemTableHead>
                        <PoemTableHead width={3}>좋아요</PoemTableHead>
                    </PoemTableRow>
                        {poemData?.poem?.map((poem: any, index: number) => (
                            <PoemTableRow height={2} key={index}> 
                                    <PoemTableData>
                                        {poem?.poemSeq}
                                    </PoemTableData>
                                    <PoemTableData onClick={() => goPoemView(poem?.poemSeq)}>
                                        {poem?.title}
                                    </PoemTableData>
                                    <PoemTableData>
                                        {poem?.writer}
                                    </PoemTableData>
                                    <PoemTableData>
                                        {moment(poem?.write_date).format('YYYY-MM-DD hh:mm:ss')}
                                    </PoemTableData>
                                    <PoemTableData>
                                        {poem?.views}
                                    </PoemTableData>
                                    <PoemTableData>
                                        {poem?.goods}
                                    </PoemTableData>
                            </PoemTableRow>
                        ))}  
                    <PoemTableRow>      
                    </PoemTableRow>
                </PoemTable>
        )
    }, [poemData?.poem]);

    return (
        <PoemGalleryMainComponent>
            <Header />
            <PoemGalleryBodyComponent>
                <PoemGalleryBodyInnerComponent>
                    {viewWritePoems()}
                    <TablePagingComponent>
                        <LeftArrow onClick={() => {
                            currentPage > 1 ? setCurrentPage(currentPage - 1) : setCurrentPage(currentPage);
                        }}/>
                        <TablePagingNumberComponent>
                           {paging?.map((page: number, index: number) => (
                               page === currentPage ? (
                                   <CurrentPagingNumber key={index}>{page}</CurrentPagingNumber>
                                ): (
                                   <PagingNumber key={index} onClick={() => setCurrentPage(page)}>{page}</PagingNumber>
                               )
                           ))}
                        </TablePagingNumberComponent>
                        <RightArrow onClick={() => {
                            currentPage < poemData?.last_paging ? setCurrentPage(currentPage + 1) : setCurrentPage(currentPage);
                        }} />
                    </TablePagingComponent>
                    <WritePoemButton onClick={() => setVisible(true)}><BiPencil />시 쓰기</WritePoemButton>
                </PoemGalleryBodyInnerComponent>
            </PoemGalleryBodyComponent>
            <WriteDisplay 
              visible={visible}
              setVisible={setVisible}
              title={title}
              setTitle={setTitle}
              writer={writer}
              setWriter={setWriter}
              content={content}
              setContent={setContent}
            />
        </PoemGalleryMainComponent>
    );
}

export default PoemGallery;