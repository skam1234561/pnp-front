import React, { useCallback, useEffect, useState } from "react";
import { 
    PoemViewBodyComponent, 
    PoemViewMainComponent, 
    PoemViewMainPoemContent, 
    PoemViewTitle, 
    PoemViewContent, 
    PoemViewImage, 
    PeomViewWriter,
    PoemGoodsTrueHeart,
    PoemGoodsFalseHeart
} from "./style/PoemViewStyled";
import Header from "../../components/header/Header";
import { useMutation, useQuery } from "react-query";
import { fetchData, postData } from "../../service/APIEngine";
import { usePoemSeqContext } from "../../store/context/PoemContext";
import { IMG_URL } from "../../config/API_APP_URL";
import { useSearchParams } from "react-router-dom";

const PoemView = () => {
    const poemSeqConext = usePoemSeqContext();
    const [searchParams] = useSearchParams();
    const poemSeq = searchParams.get('poemSeq');
    const [imageList, setImageList] = useState<any[]>([]);
    const [index, setIndex] = useState<number>(0);
    const [status, setStatus] = useState<string>('T');


    const {
        mutate: goodsMutate
    } = useMutation(
        async () => await postData(
            '/toggle-goods',
            {
                id: 'skam123',
                poemSeq: poemSeq,
            },
            false,
            '',
        ),
        {
            onSuccess: () => {
                if(statusData) {
                    console.log('좋아요 해제');

                }else {
                    console.log('좋아요 완료');
                }
                
            }
        }
    )

    const {mutate: viewsMutate } = useMutation(
        async () => await postData(
            '/user-views',
            {
                id: 'skam123',
                poemSeq: poemSeq,
            },
            false,
            '',
        ),
        {
            onSuccess: () => {
                console.log('조회수 확인');
            }
        }
    )

    const {
        data: poemData,
        error: poemError,
        isError: poemIsError,
        refetch: poemRefetch,
    } = useQuery(['read-paging-poem', [poemSeq]], async () => {
        let poem = await fetchData(
            {
                poemSeq: poemSeq,
            },
            '/read-one-poem',
        );
        viewsMutate();
        return poem?.data;
    });


    const {
        data: imageData,
        error: imageError,
        isError: imageIsErorr,
        refetch: imageRefetch,
    } = useQuery(['read-all-img', [poemData?.content]], async () => {
        let data = await fetchData(
            {
            id: 'skam123',
            },
            '/read-all-img',
        );
        setImageList(data?.data);
        return data?.data;
    });

    const {
        data: statusData,
        error: statusError,
        isError: statusIsError,
        refetch: statusRefetch,
    } = useQuery(['read-goods-stat', [poemSeq, status]], async () => {
        let status  = await fetchData(
            {
                id: 'skam123',
                poemSeq: poemSeq,
            },
            '/read-goods-stat',
        );
        console.log(status?.data?.status);
        return status?.data?.status;
    })

    const goodsClick = useCallback(() => {
        if(status === 'T') {
            setStatus('F')
        }else {
            setStatus('T');
        }
        goodsMutate();
    }, [goodsMutate, status]);
    


    useEffect(() => {
        if(imageData) {
            let length = imageList?.length;
            let indexes = Math.floor(Math.random() * length);
            setIndex(indexes);
        }
    }, [imageData])


    return (
        <PoemViewMainComponent>
            {
                statusData === undefined ?
                (<PoemGoodsFalseHeart onClick={() => goodsClick()} />) :
                null
            }
            {
                statusData ? 
                (<PoemGoodsTrueHeart onClick={() => goodsClick()} />)
                :
                (<PoemGoodsFalseHeart onClick={() => goodsClick()} />)
            }
            <Header />
            <PoemViewBodyComponent>
                <PoemViewMainPoemContent>
                    <PoemViewTitle length={poemData?.title?.length}>{poemData?.title}</PoemViewTitle>
                    <PeomViewWriter>{poemData?.writer}</PeomViewWriter>
                    <PoemViewContent>{poemData?.content}</PoemViewContent>
                </PoemViewMainPoemContent>
                <PoemViewImage imagePath={`${IMG_URL}${imageList[index]}`} />
            </PoemViewBodyComponent>
        </PoemViewMainComponent>
    );
}

export default PoemView;