import React, { ReactNode, createContext, useContext, useState } from "react";

interface PoemContextProps {
    poemSeq: number;
    setPoemSeq: React.Dispatch<React.SetStateAction<number>>;
}

interface ChildrenProps {
    children: ReactNode;
}

// createContext의 기본값을 PoemContextProps의 형태에 맞춰 설정
const PoemContext = createContext<PoemContextProps | undefined>(undefined);

const PoemProvider = ({ children }: ChildrenProps) => {
    const [poemSeq, setPoemSeq] = useState<number>(0);
    return (
        <PoemContext.Provider value={{ poemSeq, setPoemSeq }}>
            {children}
        </PoemContext.Provider>
    );
}

const usePoemSeqContext = () => {
    const context = useContext(PoemContext);
    if (context === undefined) {
        throw new Error('usePoemSeqContext must be used within a PoemProvider');
    }
    return context;
}

export { PoemContext, PoemProvider, usePoemSeqContext };