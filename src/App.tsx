import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import Router from './route/Router';
import { PoemProvider } from './store/context/PoemContext';

const queryClient = new QueryClient();
const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <PoemProvider>
        <Router />
      </PoemProvider>
    </QueryClientProvider>
  );
};

export default App;
